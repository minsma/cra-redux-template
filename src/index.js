import React from 'react';
import './index.css';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';
import MainPage from './MainPage';
import { HashRouter, Route, Switch } from 'react-router-dom';
import registerServiceWorker from './registerServiceWorker';
import GriddleTablePage from './GriddleTablePage';

render(
    <Provider store={store}>
        <HashRouter>
            <Switch>
                <Route path="/griddle" component={GriddleTablePage} />
                <Route exact path="/" component={MainPage} />
            </Switch>
        </HashRouter>
    </Provider>, document.getElementById('root')
);
registerServiceWorker();

